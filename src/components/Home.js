import React from 'react';
import Navigate from "./Navigate";
import Login from "./Login";
import styled from "styled-components";

const Anchor = styled.a`
  height: 40px; 
  position: fixed; 
  bottom:0%;
  
`;

const MyComponent = () => {
    return (
        <div className="App">
            <br></br>
            <h3> Matthew Pieri's Virtual Plant App</h3>
            <p></p>
            {/*<a href="Resume_2021.pdf" download="Resume_2021.pdf">Download Resume</a>*/}
            <Login/>
            <Anchor>
            <a href="Resume_2021.pdf" target="_blank"> Resume</a>
            </Anchor>
            {/*<Navigate/>*/}

        </div>
    );
};

export default MyComponent;
