import React, {Suspense, useEffect, useRef} from "react";
import App from "../App";
import {RecoilRoot} from "recoil";
import {Canvas, useFrame, useThree, useLoader } from "@react-three/fiber";
import {OrbitControls} from "three/examples/jsm/controls/OrbitControls";
import {GLTFLoader} from "three/examples/jsm/loaders/GLTFLoader";
import { useAnimations } from "@react-three/drei";
const GROUND_HEIGHT = 0;
function Terrain() {
    const terrain = useRef();

    useFrame(() => {
    })
    // Returns a mesh at GROUND_HEIGHT below the player. Scaled to 5000, 5000 with 128 segments.
    // X Rotation is -Math.PI / 2 which is 90 degrees in radians.
    return (
        <mesh
            visible
            position={[0, GROUND_HEIGHT, 0]}
            rotation={[-Math.PI / 2, 0, 0]}
            ref={terrain}
        >
            <planeBufferGeometry attach="geometry" args={[150, 150, 128, 128]} />
            <meshStandardMaterial
                color="0x202020"
            />
        </mesh>
    );
}

function Loading() {
    return (
        <mesh visible position={[0, 0, 0]} rotation={[0, 0, 0]}>
            <sphereGeometry attach="geometry" args={[1, 16, 16]} />
            <meshStandardMaterial
                attach="material"
                color="white"
                transparent
                opacity={0.6}
                roughness={1}
                metalness={0}
            />
        </mesh>
    );
}

const CameraController = () => {
    const { camera, gl } = useThree();
    useEffect(
        () => {
            const controls = new OrbitControls(camera, gl.domElement);
            controls.minDistance = 3;
            controls.maxDistance = 125;
            return () => {
                controls.dispose();
            };
        },
        [camera, gl]
    );
    return null;
};
/*
function Scene() {
    const fbx = useLoader(FBXLoader, '/Ninja Idle.fbx')
    return <primitive
        object={fbx}
        position={[0, GROUND_HEIGHT, 0]}
        scale = {[.2, .2, .2]}
    />
}
*/
function Wrestler(props) {
    const group = useRef()
    const { nodes, materials, animations } = useLoader( GLTFLoader,'models/wrestler_2.glb')
    const { actions } = useAnimations(animations, group)
    useEffect(() => {
        actions.kick.play()
    })
    return (
        <group ref={group} {...props} dispose={null}>
            <group name="Armature" position = {[0, GROUND_HEIGHT, 0]} rotation={[Math.PI / 2, 0, 0]} scale={[4,4,4]}>
                <primitive object={nodes.mixamorigHips} />
                <skinnedMesh
                    geometry={nodes.Ch43.geometry}
                    material={materials.Ch43_Body}
                    skeleton={nodes.Ch43.skeleton}
                />
            </group>
        </group>
    )
}

function Plant(props) {
    const group = useRef()
    const { nodes, materials } = useLoader( GLTFLoader,'models/PLANT-BASICS_JOINED.glb')
    return (
        <group ref={group} {...props} dispose={null}>
            <group ref={group} {...props} dispose={null}>
                <group position={[0.51, GROUND_HEIGHT+4, -0.15]} rotation={[0,3.8, 0]} scale={[4, 4, 4]}>
                    <mesh
                        castShadow
                        receiveShadow
                        geometry={nodes.NurbsPath_1.geometry}
                        material={materials['Material.005']}
                    />
                    <mesh
                        castShadow
                        receiveShadow
                        geometry={nodes.NurbsPath_2.geometry}
                        material={materials['Material.002']}
                    />
                    <mesh
                        castShadow
                        receiveShadow
                        geometry={nodes.NurbsPath_3.geometry}
                        material={materials['Material.001']}
                    />
                    <mesh
                        castShadow
                        receiveShadow
                        geometry={nodes.NurbsPath_4.geometry}
                        material={materials['Material.003']}
                    />
                    <mesh
                        castShadow
                        receiveShadow
                        geometry={nodes.NurbsPath_5.geometry}
                        material={materials['Material.004']}
                    />
                </group>
            </group>
        </group>
    )
}


export default class RpgApp extends React.Component {

    constructor(props) {
        super(props);
        this.state = {showPlaneGame: false,
            showRpgGame: true
        };

        // This binding is necessary to make `this` work in the callback
    }


    render() {
        return (
            <div className={'App'}>
                <Canvas camera={{position: [0, 0, 125]}} style={{background: "\t#8cd3ff"}}>
                    <CameraController />
                    <directionalLight intensity={.20}/>
                    <ambientLight intensity={0.70}/>
                    <pointLight position={[20, 20, 20]}/>
                    <Terrain/>

                    <Suspense fallback={<Loading/>}>
                        <Plant></Plant>
                        {/*} <Wrestler/>*/}
                    </Suspense>
                </Canvas>

            </div>
        );
    }
}