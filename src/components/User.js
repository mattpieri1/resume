import React, {Suspense, useEffect, useRef, useState} from 'react';
import axios from "axios";
import {GLTFLoader} from "three/examples/jsm/loaders/GLTFLoader";
import {Canvas, useFrame, useThree, useLoader } from "@react-three/fiber";
import { styled as muistyled } from '@mui/material/styles';
import styled from 'styled-components'
import { purple } from '@mui/material/colors';
import {
    Box,
    Button,
    Card,
    List,
    ListItem,
    ListItemButton,
    ListItemIcon,
    ListItemText, Stack,
    SwipeableDrawer
} from "@mui/material";
import {OrbitControls} from "three/examples/jsm/controls/OrbitControls";
const GROUND_HEIGHT = -20;

const ColorButton = muistyled(Button)(({ theme }) => ({
    backgroundColor: '#FFFFFF',
    fontSize: 50,
    width:  '120px',
}));

const CenteredButton = muistyled(Button)(({ theme }) => ({
    position: 'fixed',
    fontSize: 50,
}));

const FlexDiv = styled.div`
  display: flex;
    justify-content: center;
    align-items: center;
`;



function Loading() {
    return (
        <mesh visible position={[0, 0, 0]} rotation={[0, 0, 0]}>
            <sphereGeometry attach="geometry" args={[1, 16, 16]} />
            <meshStandardMaterial
                attach="material"
                color="white"
                transparent
                opacity={0.6}
                roughness={1}
                metalness={0}
            />
        </mesh>
    );
}

const CameraController = () => {
    const { camera, gl } = useThree();
    useEffect(
        () => {
            const controls = new OrbitControls(camera, gl.domElement);
            controls.minDistance = 3;
            controls.maxDistance = 125;
            return () => {
                controls.dispose();
            };
        },
        [camera, gl]
    );
    return null;
};

function Plant(props) {
    const group = useRef()
    const { nodes, materials } = useLoader( GLTFLoader,'models/PLANT-BASICS_JOINED.glb')

    useFrame(() => {
        group.current.rotation.y += 0.004;
    })
    return (
        <group ref={group} {...props} dispose={null}>
            <group ref={group} {...props} dispose={null}>
                <group position={[0.51, GROUND_HEIGHT, -0.15]} rotation={[0,3.8, 0]} scale={[15, 15, 15]}>
                    <mesh
                        castShadow
                        receiveShadow
                        geometry={nodes.NurbsPath_1.geometry}
                        material={materials['Material.005']}
                    />
                    <mesh
                        castShadow
                        receiveShadow
                        geometry={nodes.NurbsPath_2.geometry}
                        material={materials['Material.002']}
                    />
                    <mesh
                        castShadow
                        receiveShadow
                        geometry={nodes.NurbsPath_3.geometry}
                        material={materials['Material.001']}
                    />
                    <mesh
                        castShadow
                        receiveShadow
                        geometry={nodes.NurbsPath_4.geometry}
                        material={materials['Material.003']}
                    />
                    <mesh
                        castShadow
                        receiveShadow
                        geometry={nodes.NurbsPath_5.geometry}
                        material={materials['Material.004']}
                    />
                </group>
            </group>
        </group>
    )
}


function InboxIcon() {
    return null;
}

const User = () => {
    const [user, setUser] = useState(null)
    const [state, setState] = React.useState({
        top: false,
        left: false,
        bottom: false,
        right: false,
    });

    const toggleDrawer = (anchor, open) => (event) => {
        if (
            event &&
            event.type === 'keydown' &&
            (event.key === 'Tab' || event.key === 'Shift')
        ) {
            return;
        }

        setState({ ...state, [anchor]: open });
    };

    const list = (anchor) => (
        <Stack direction="row"   >
            <ListItem><Card>Item 1</Card>Water</ListItem>
            <ListItem><Card>Item 1</Card></ListItem>
            <ListItem><Card>Item 1</Card></ListItem>
            <ListItem><Card>Item 1</Card></ListItem>
            <ListItem><Card>Item 1</Card></ListItem>
        </Stack>
    );

    useEffect(() => {
        axios({
            method: "GET",
            withCredentials: true,
            url: process.env.REACT_APP_BACKEND_URL_BASE + "/user"
        }).then(data => {
            setUser(data)
            console.log(data)
        } )

    }, [] );


    return (
        <div>
        {
        user &&
         <div>
             <div style={{background: "\t#8cd3ff", height: "21vh" }}>
             <div >Welcome, {user.data.username} </div>
             <div >Age: 25 Days </div>
             <div >Happiness: &#128512; &#128512; &#128512; &#128512; </div>
             <div >Sick: &#129314; &#129314;  </div>
             <div >Life: &#129505; &#129505; &#129505; &#129505; </div>
             <div >Water: &#128167; &#128167; &#128167; &#128167; </div>
             <div >Light: &#128161; &#128161;   </div>
             <div>Carbon: &#x1F4A8; &#x1F4A8; &#x1F4A8;    </div>

             </div>
                <Canvas camera={{position: [0, 0, 125]}} style={{background: "\t#8cd3ff", height: "74vh" }}>
                    <directionalLight intensity={.20}/>
                    <ambientLight intensity={0.70}/>
                    <pointLight position={[20, 20, 20]}/>
                    <Suspense fallback={<Loading/>}>
                        <Plant></Plant>
                    </Suspense>
                </Canvas>
             <FlexDiv>
                 <CenteredButton onClick={toggleDrawer('bottom', true)}>&#x1F446;</CenteredButton>
             </FlexDiv>
             <div style={{background: "\t#8cd3ff", height: "5vh" }}>
                     <React.Fragment key={'bottom'}>

                         <SwipeableDrawer
                             anchor={'bottom'}
                             open={state['bottom']}
                             onClose={toggleDrawer('bottom', false)}
                             onOpen={toggleDrawer('bottom', true)}
                         >
                             <Stack direction="row"  style={{height: "20vh" }}  >
                                 <ListItem ><ColorButton variant="contained"> &#128167;</ColorButton></ListItem>
                                 <ListItem ><ColorButton variant="contained"> &#x1F4A8;</ColorButton></ListItem>
                                 <ListItem ><ColorButton variant="contained"> &#128161;</ColorButton></ListItem>
                                 <ListItem ><ColorButton variant="contained"> &#127790;</ColorButton></ListItem>
                                 <ListItem ><ColorButton variant="contained"> &#128135;</ColorButton></ListItem>
                                 <ListItem ><ColorButton variant="contained"> &#127918;</ColorButton></ListItem>
                                 <ListItem ><ColorButton variant="contained"> &#129529;</ColorButton></ListItem>
                             </Stack>
                         </SwipeableDrawer>
                     </React.Fragment>
             </div>

         </div>
        }
        </div>
    );
};

export default User;
