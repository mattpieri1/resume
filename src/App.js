import logo from './logo.svg';
import './App.css';
import React, { Suspense, useRef, useState, useLayoutEffect, useEffect } from 'react'
import { RecoilRoot, useRecoilState, useRecoilValue } from "recoil";
import { GLTFLoader } from "three/examples/jsm/loaders/GLTFLoader";
import { OrbitControls } from "three/examples/jsm/controls/OrbitControls";

import * as THREE from "three";
import { Canvas, useFrame, useLoader, useThree  } from '@react-three/fiber';
import {
    shipPositionState,
    enemyPositionState,
    laserPositionState,
    planePositionState,
    scoreState
} from "./gameState";
const GROUND_HEIGHT = -50;
const ENEMY_SPEED = 0.1;
function Box(props) {
    // This reference will give us direct access to the mesh
    const mesh = useRef()
    // Set up state for the hovered and active state
    const [hovered, setHover] = useState(false)
    const [active, setActive] = useState(false)
    // Rotate mesh every frame, this is outside of React without overhead
    useFrame(() => (mesh.current.rotation.x += 0.01))

    return (

        <mesh
            {...props}
            ref={mesh}
            scale={active ? 1.5 : 1}
            onClick={(event) => setActive(!active)}
            onPointerOver={(event) => setHover(true)}
            onPointerOut={(event) => setHover(false)}>
            <boxGeometry args={[1, 2, 3]} />
            <meshStandardMaterial color={hovered ? 'hotpink' : 'orange'} />
        </mesh>
    )
}

function Terrain() {
    const terrain = useRef();

    useFrame(() => {
        terrain.current.position.z += 0.4;
    })
    // Returns a mesh at GROUND_HEIGHT below the player. Scaled to 5000, 5000 with 128 segments.
    // X Rotation is -Math.PI / 2 which is 90 degrees in radians.
    return (
        <mesh
            visible
            position={[0, GROUND_HEIGHT, 0]}
            rotation={[-Math.PI / 2, 0, 0]}
            ref={terrain}
        >
            <planeBufferGeometry attach="geometry" args={[5000, 5000, 128, 128]} />
            <meshStandardMaterial
                attach="material"
                color="\t#8cd3ff"
                roughness={1}
                metalness={0}
                wireframe
            />
        </mesh>
    );
}

function ArWing() {
    const ship = useRef();

    const [shipPosition, setShipPosition] = useRecoilState(shipPositionState);
    useFrame(({ mouse }) => {
        setShipPosition({
            position: { x: mouse.x * 6, y: 0 },
            rotation: { z: -mouse.x * 0.5, x: -mouse.x * 0.5, y: -mouse.y * 1 }
        });
    });
    // Update the ships position from the updated state.
    useFrame(() => {
        ship.current.rotation.z = shipPosition.rotation.z;
        ship.current.rotation.y = shipPosition.rotation.x;
        ship.current.rotation.x = shipPosition.rotation.y;
        ship.current.position.y = shipPosition.position.y;
        ship.current.position.x = shipPosition.position.x;
    });

    const { nodes, materials  } = useLoader(GLTFLoader, "models/lowpolyairplane.glb");
    return (
        <group ref={ship} dispose={null}>
            <group position={[0, 0, -2.1]} rotation={[Math.PI / 2, 0, 0]} scale={[0.5, 0.5, 0.5]}>
                <mesh
                   // onPointerDown={(e) => console.log("down")}

                    castShadow
                    receiveShadow
                    geometry={nodes.Circle_1.geometry}
                    material={materials['Material.001']}
                />
                <mesh
                    castShadow
                    receiveShadow
                    geometry={nodes.Circle_2.geometry}
                    material={materials['Material.006']}
                />
            </group>
            <mesh
                castShadow
                receiveShadow
                geometry={nodes.Circle001.geometry}
                material={materials['Material.005']}
                position={[0, 0, -2.1]}
                rotation={[Math.PI / 2, 0, 0]}
                scale={[0.5, 0.5, 0.5]}
            />
            <mesh
                castShadow
                receiveShadow
                geometry={nodes.Circle002.geometry}
                material={materials['Material.003']}
                position={[0, 0, -2.1]}
                rotation={[Math.PI / 2, 0, 0]}
                scale={[0.5, 0.5, 0.5]}
            />
            <mesh
                castShadow
                receiveShadow
                geometry={nodes.Plane.geometry}
                material={materials['Material.004']}
                position={[0, 1.17, 0.24]}
            />
        </group>
    );
}

function Loading() {
    return (
        <mesh visible position={[0, 0, 0]} rotation={[0, 0, 0]}>
            <sphereGeometry attach="geometry" args={[1, 16, 16]} />
            <meshStandardMaterial
                attach="material"
                color="white"
                transparent
                opacity={0.6}
                roughness={1}
                metalness={0}
            />
        </mesh>
    );
}

function GameTimer() {
    const [enemies, setEnemies] = useRecoilState(enemyPositionState);
    setEnemies(
        enemies
            .map((enemy) => ({ x: enemy.x, y: enemy.y, z: enemy.z + ENEMY_SPEED }))
            .filter((enemy, idx) => !true && enemy.z < 0)
    );
    return null;
}

function textMesh( x, y, z, nodes, ref ) {
    return (
        <group ref={ref}  dispose={null}>
            <mesh
                castShadow
                receiveShadow
                position={[x, y, z]}
                rotation={[Math.PI / 2, 0, 0]}
                geometry={nodes.Text.geometry}
                material={nodes.Text.material}
            />
        </group>
    )
}

function Dolly() {
    // This one makes the camera move in and out
    useFrame(({ clock, camera, mouse }) => {
        //camera.position.z = 50 + Math.sin(clock.getElapsedTime()) * 30
        camera.position.y = -mouse.y * .05 + camera.position.y
    })
    return null
}

function GoldmanSachs() {
    const group = useRef()
    const { nodes, materials } = useLoader(GLTFLoader, 'models/GS.glb')
    useFrame(() => (group.current.position.z += 0.09))
    return textMesh(-5, 2, -150, nodes, group)
}

function BankOfAmerica() {
    const group = useRef()
    const { nodes, materials } = useLoader(GLTFLoader, 'models/baml.glb')
    useFrame(() => (group.current.position.z += 0.09))
    return textMesh(4, 2, -120, nodes, group)
}

function HofstraUniversity() {
    const group = useRef()
    const { nodes, materials } = useLoader(GLTFLoader, 'models/Hofstra.glb')
    useFrame(() => (group.current.position.z += 0.09))
    return textMesh(-5, 2, -80, nodes, group)
}

function Ring(props) {
    const ref = useRef(null);
    useFrame(({ mouse,onPointerMissed }) => {
        //console.log(mouse.y + " mouse ")
        //console.log(onPointerMissed)
        ref.current.position.z += .2;
        ref.current.position.y +=  mouse.y * .3;
        ref.current.rotation.z += .01
    });

    return (
        <mesh
            visible
            position={[props.positionX +1, props.positionY + 1 , -props.positionZ + -100]}
            rotation={[0, 0, 0]}
            castShadow
            ref={ref}>
            <ringBufferGeometry args={[5.5, 6, 10]} />
            <meshBasicMaterial attach="material" color="#E72C1A" />
        </mesh>
    );
}

function Plane(){
    const group = useRef()

    const [planePosition, setPlanePosition] = useRecoilState(planePositionState);
    useFrame(({ mouse }) => {
        setPlanePosition({
            position: { x: mouse.x * 6, y: mouse.y * .3},
        });
    });
    // Update the ships position from the updated state.
    useFrame(() => {
        group.current.position.z += 0.04;
        group.current.position.y += planePosition.position.y;
    });

    const { nodes, materials } = useLoader(GLTFLoader, 'models/landscape_big_block_1.glb')

    return (
        //        <group ref={group} dispose={null} scale={[7,3,1]} position={[0,-20,-57]}>
        <group ref={group}  dispose={null}>
            <group position={[0, -30, -100]} scale={[2,2,2]} >
                <mesh
                    castShadow
                    receiveShadow
                    geometry={nodes.Plane001_1.geometry}
                    material={materials['Material.005']}
                />
                <mesh
                    castShadow
                    receiveShadow
                    geometry={nodes.Plane001_2.geometry}
                    material={materials['Material.003']}
                />
                <mesh
                    castShadow
                    receiveShadow
                    geometry={nodes.Plane001_3.geometry}
                    material={materials['Material.004']}
                />
                <mesh
                    castShadow
                    receiveShadow
                    geometry={nodes.Plane001_4.geometry}
                    material={materials['Material.006']}
                />
                <mesh
                    castShadow
                    receiveShadow
                    geometry={nodes.Plane001_5.geometry}
                    material={materials['Material.007']}
                />
            </group>

        </group>
    )
}

function randomIntFromInterval(min, max) { // min and max included
    return Math.floor(Math.random() * (max - min + 1) + min)
}

const rndInt = randomIntFromInterval(1, 6)

function ringMap() {
    var map = new Object();
    for (let i = 0; i < 40; i++) {
        var node = new Object;
        node["positionZ"] = i + 50;

        map[i] = node;
    }
    return map;
}

const CameraController = () => {
    const { camera, gl } = useThree();
    useEffect(
        () => {
            const controls = new OrbitControls(camera, gl.domElement);
            controls.minDistance = 3;
            controls.maxDistance = 20;
            return () => {
                controls.dispose();
            };
        },
        [camera, gl]
    );
    return null;
};

export default class App extends React.Component {
    /*let myMap = [];
    for (let i = 0; i < 50; i++) {
        let node = new Map();
        node.set("positionZ", i * 50);
        node.set("positionX", randomIntFromInterval(-7, 7));
        node.set("positionY", randomIntFromInterval(-17, 20));
        myMap[i]= node;
    }*/

    constructor(props) {
        super(props);
        this.state = {pointerTest: 0,
                      ringMapping: this.getRingMapping()
        };

        // This binding is necessary to make `this` work in the callback
        this.getRingMapping = this.getRingMapping.bind(this);
        this.pointerDown = this.pointerDown.bind(this);
    }

    getRingMapping() {
        let myMap = [];
        for (let i = 0; i < 50; i++) {
            let node = new Map();
            node.set("positionZ", i * 50);
            node.set("positionX", randomIntFromInterval(-7, 7));
            node.set("positionY", randomIntFromInterval(-17, 20));
            myMap[i] = node;
        }
        return myMap
    }

    pointerDown(e) {
        console.log(e)
        this.setState(prevState => ({
            pointerTest: prevState.pointerTest+1
        }));
        return
    }


    render() {
        return (
                <Canvas camera={{position: [0, 0, 10]}} style={{background: "\t#8cd3ff"}}>
                    {/*<CameraController />*/}
                    <RecoilRoot>

                        <directionalLight intensity={.05}/>
                        <ambientLight intensity={0.25}/>
                        <pointLight position={[10, 10, 10]}/>
                        <Suspense fallback={<Loading/>}>
                            <ArWing/>
                        </Suspense>
                        <Suspense fallback={<Loading/>}>
                            <GoldmanSachs/>
                        </Suspense>
                        <Suspense fallback={<Loading/>}>
                            <HofstraUniversity/>
                        </Suspense>
                        <Suspense fallback={<Loading/>}>
                            <BankOfAmerica/>
                        </Suspense>
                        <Suspense fallback={<Loading/>}>
                            <Plane

                            />
                        </Suspense>
                        {this.state.ringMapping.map((test, number) =>
                            <Ring key={number} positionZ={test.get("positionZ")} positionX={test.get("positionX")}
                                  positionY={test.get("positionY")}/>
                        )}
                        <Terrain/>
                        <Dolly/>
                    </RecoilRoot>
                </Canvas>
        );
    }
}