import React, { useState }from 'react';
import axios, {Axios} from 'axios';
import {Button, Card, Stack, TextField} from "@mui/material";
import styled from 'styled-components'
import { useHistory } from 'react-router-dom'

const Styled_Card = styled(Card)`
  color: blue;
  background-color: blue;
  width: 300px;
  margin: auto;
  padding: 10px;
`;

const Title = styled.h1`
  font-size: 1.5em;
  font-family: Segoe UI;
  text-align: center;
  color: black;
`;
const MyComponent = () => {
    const history = useHistory();
    const [registerUsername, setRegisterUsername] = useState("")
    const [registerPassword, setRegisterPassword] = useState("")
    const [loginUsername, setLoginUsername] = useState("")
    const [loginPassword, setLoginPassword] = useState("")
    const [hideLogin, setHideLogin] = useState(true)
    const register = () => {
        axios({
            method: "post",
            data: {
                username: registerUsername,
                password: registerPassword
            },
            withCredentials: true,
            url: process.env.REACT_APP_BACKEND_URL_BASE + "/register"
        })
            .then(res => console.log(res))
    };
    const login = () => {
        axios({
            method: "post",
            data: {
                username: loginUsername,
                password: loginPassword
            },
            withCredentials: true,
            url:process.env.REACT_APP_BACKEND_URL_BASE + "/login"
        })
            .then(res => history.push(`/user`))
    };
    const getUser = () => {
        axios({
            method: "GET",
            withCredentials: true,
            url: process.env.REACT_APP_BACKEND_URL_BASE + "/user"
        }).then((res) => console.log(res));
    };
    return (
        <div>
            {hideLogin &&
            <div>
                <Title>Sign Up</Title>
                <Styled_Card>
                    <Stack spacing={3}>
                        <TextField   id="standard-basic" label="Username" variant="standard" onChange={e => setRegisterUsername(e.target.value)}/>
                        <TextField   id="standard-basic" label="Password" variant="standard" onChange={e => setRegisterPassword(e.target.value)}/>
                        <Button variant="contained" onClick={register}>Create Account </Button>
                    </Stack>
                    <p onClick={e => setHideLogin(false)}>Sign in</p>
                </Styled_Card>
            </div>
            }
            {!hideLogin &&
            <div>
                <Title>Login</Title>
                <Styled_Card>
                    <Stack spacing={3}>
                        <TextField id="standard-basic" label="Username" variant="standard"
                                   onChange={e => setLoginUsername(e.target.value)}/>
                        <TextField id="standard-basic" label="Password" variant="standard"
                                   onChange={e => setLoginPassword(e.target.value)}/>
                        <Button variant="contained" onClick={login}>Login</Button>
                    </Stack>
                    <p onClick={e => setHideLogin(true)}>Sign Up</p>
                </Styled_Card>
            </div>}
        </div>
    );
};

export default MyComponent;
