import React, {Suspense} from "react";
import {Canvas} from "@react-three/fiber";
import {RecoilRoot} from "recoil";
import App from "../App";
import RpgApp from "./RpgApp";
import Login from "./Login";



export default class Navigate extends React.Component {

    constructor(props) {
        super(props);
        this.state = {showPlaneGame: false,
            showRpgGame: true
        };

        // This binding is necessary to make `this` work in the callback
        this.togglePlaneGame = this.togglePlaneGame.bind(this);
        this.toggleRpgGame = this.toggleRpgGame.bind(this);
    }

    togglePlaneGame(e) {
        console.log(e)
        this.setState(prevState => ({
            showPlaneGame: true,
            showRpgGame: false
        }));
        return
    }

    toggleRpgGame(e) {
        console.log(e)
        this.setState(prevState => ({
            showPlaneGame: false,
            showRpgGame: true
        }));
        return
    }


    render() {
        return (
            <div className={'App'}>
                <button style={{marginRight: '10px'}} onClick={this.togglePlaneGame}>Plane Game</button>
                <button onClick={this.toggleRpgGame}>Rpg Game</button>
                {this.state.showPlaneGame  ? <App /> : null}
                {this.state.showRpgGame  ? <RpgApp />: null}
            </div>
        );
    }
}