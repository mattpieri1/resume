import React from 'react';
import Navigate from "./Navigate";
import {
    BrowserRouter as Router,
    Switch,
    Route,
} from "react-router-dom";
import Home from "./Home";
import User from "./User";

const App = () => {
    return (
        <Router>
            <div>
                  <Switch >
                    <Route exact path="/">
                        <Home />
                    </Route>
                    <Route path="/User">
                        <User />
                    </Route>
                </Switch >
            </div>
        </Router>
    );
};

export default App;
